import os

IMG_SCRAPE_DIR_PATH = "learning-tools/img/original"
FACE_EXTRACTION_DIR_PATH = "learning-tools/img/face-only"
DATA_AUGMENTATION_TRAIN_DIR_PATH = "learning-tools/img/argmentation/train"
DATA_AUGMENTATION_TEST_DIR_PATH = "learning-tools/img/argmentation/test"
PREDICT_DATA_ORIGINAL_PATH = "learning-tools/img/predict/original"
PREDICT_DATA_PATH = "learning-tools/img/predict/face-only"

def recursive_file_check(path, image_files):
    if os.path.isdir(path):
        files = os.listdir(path)
        for file in files:
            recursive_file_check(path + "/" + file, image_files)
    else:
        image_files.append(path)

def make_directory(image_files, input_dir, output_dir):
    image_file_dirs = {image_file.replace('\\', '/').rsplit('/', 1)[0] for image_file in image_files}
    for dir in image_file_dirs:
        os.makedirs(dir.replace(input_dir, output_dir), exist_ok=True)