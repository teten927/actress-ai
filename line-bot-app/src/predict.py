import os
import sys
import glob
from PIL import Image
import dlib
import imageio
import numpy as np
os.environ['KERAS_BACKEND'] = 'tensorflow'
from tensorflow.keras.models import load_model
import gc

def face_extract(img, cropped_imgs):
    try:
        detector = dlib.get_frontal_face_detector()
        img = np.asarray(Image.open(img))
        open_img = Image.fromarray(img)
        dets = detector(img, 1)
        dets = sorted(dets, key=lambda det: det.left())

        for index, det in enumerate(dets):
            cropped_imgs.append(open_img.crop((det.left(), det.top(), det.right(), det.bottom())).resize((96, 96)))
            if index > 2:
                return -1
        return 1

    except Exception as e:
        print(e)
        raise

def predict(imgs, results):
    try:
        model = load_model('model/model.h5')
        for img in imgs:
            X = []
            img = np.asarray(img, dtype='float32')
            img /= 255
            X.append(img.tolist())
            Y = model.predict([X])
            index = np.argmax(Y, axis=1)
            with open("data/data.txt", encoding='shift-jis', mode='r') as f:
                results.append(f.readlines()[index[0]].rstrip('\n'))
    except Exception as e:
        print(e)
        raise
    finally:
        del model
        gc.collect()