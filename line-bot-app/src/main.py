# coding: cp932

from flask import Flask, request, Response, abort
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    ImageMessage
)
import os
from io import BytesIO
import predict

app = Flask(__name__)

CHANNEL_ACCESS_TOKEN = os.environ["CHANNEL_ACCESS_TOKEN"]
CHANNEL_SECRET = os.environ["CHANNEL_SECRET"]

line_bot_api = LineBotApi(CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(CHANNEL_SECRET)

@app.route("/callback", methods=['POST'])
def callback():

    signature = request.headers['X-Line-Signature']

    body = request.get_data(as_text=True)

    print("body:", body)

    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    except:
        return Response(status=200)

    return 'OK'

@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):

    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text="顔画像を送ってね！")
    )

@handler.add(MessageEvent, message=ImageMessage)
def handle_image(event):
    line_bot_api.push_message(
        event.source.user_id,
        [TextSendMessage(text='考え中だよ！'),
        TextSendMessage(text='.........')]
    )
    message_id = event.message.id
    message_content = line_bot_api.get_message_content(message_id)
    image = BytesIO(message_content.content)

    try:
        face_images = []
        if predict.face_extract(image, face_images) == -1:
            line_bot_api.push_message(
                event.source.user_id,
                TextSendMessage(text='4人までしか読み込めません！')
                )

        if face_images == []:
            message = TextSendMessage(text= "顔が映った画像を送ってね！")
        else:
            actress_names=[]
            predict.predict(face_images, actress_names)

            text = ""
            text_messages = []
            exist = []

            if len(actress_names) > 1:
                text = "左から順に、"
            for actress_name in actress_names:
                if len(actress_names) > 1:
                    text += "\n"                
                text += actress_name
                if actress_name not in exist:
                    text_messages.append(TextSendMessage(text = "https://www.google.com/search?q={}&aqs=chrome..69i57j0l7.1324j0j8&sourceid=chrome&ie=UTF-8".format(actress_name)))
                    exist.append(actress_name)
            if len(actress_names) > 1:
                text += "\n"
            
            message = [TextSendMessage(text = "{}にそっくり！".format(text))] + text_messages
    
    except:
        message = TextSendMessage(text= "！エラー発生！")
    
    finally:
        line_bot_api.reply_message(
            event.reply_token,
            message
        )        

if __name__ == "__main__":
    port = int(os.getenv("PORT", 5000))
    app.run(host="0.0.0.0", port=port)